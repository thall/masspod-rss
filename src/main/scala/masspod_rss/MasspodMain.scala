package masspod_rss

import sys.process._

import scala.xml._
import scala.io.Source

/**
  Header goes here.. Masspod RSS
  bla bla blah
  **/

object MasspodMain {

/**
  START OF MAIN FUNCTION
  READ CONFIGURATION FILE
  Variables are the variables set by the configuration file.
  Some may have hardcoded default values which the file overrides.
  * */
  private var channelLink = """http://www.website.com/"""
  private var channelDesc = """example channel description which should be changed"""
  private var channelName = """masspod-rss feed"""
  private var channelImage = ""
  private var channelImageName = ""
  private var channelImageWidth = 96
  private var channelImageHeight = 96
  private var outputFeedFileName = "feed.rss"
  private var feedSourceFile = ""
  private var oldNum = 80
  private var titleFilters = List[String]()
  private var outputDirectory = ""

  def main(args:Array[String]) = {
    /** Various checks **/
    // check that the config is accessable
    if(args.length!=1) Tsys.exitError("usage: masspod-rss feed.config")
    try { // check configuration file
      val config = Source.fromFile(args(0)).getLines.toList
      parseConfig(config) // TODO: CHANGE PARSING FUNCTION
    } catch {
      case e: Throwable => Tsys.exitError("Malformed, absent, or inaccessable configuration file.")
    }
    // check the feed source file for accessibility
    val sourceFileExists = Tsys.fileCheck(feedSourceFile)
    if (!sourceFileExists) Tsys.exitError("The file "+feedSourceFile+ "from \"source\" in the configuration file was not found.")
    // check the formatting of the feed source file
    // TODO: filter sourceUrls for sources which don't have any episodes within config parameters
    val sourceUrls = Source.fromFile(feedSourceFile).getLines.toList
    // test whether the source urls work
    try {val testSources = sourceUrls.map(sourceUrl => XML.load(sourceUrl))} catch {
      case a: org.xml.sax.SAXParseException => Tsys.exitError("Malformed \"source\" file "+feedSourceFile+" , check for extra whitespace")
    }
    /** Here the fun begins **/
    // create the feeds
    val epSequences = sourceUrls.map(sourceUrl => Process.processFeed(XML.load(sourceUrl))).toSeq
    var masterEpisodeSequence:Seq[Episode] = Seq()
    // add every episode to the master sequence
    for (epSeq <- epSequences; ep <- epSeq) {
      masterEpisodeSequence = masterEpisodeSequence :+ ep
    }
    // sort all the episodes by publication date
    masterEpisodeSequence = Process.sortByDate(masterEpisodeSequence)
    // create a master feed
    val masterFeed = new Feed(channelName,channelLink,masterEpisodeSequence,outputDirectory)
    // remove old episodes and delete their copies on disk
    masterFeed.filterEps(oldNum, titleFilters)
    // download the episodes to the disk
    masterFeed.downloadAll(outputDirectory, channelLink)
    println("Finished downloading episodes")
    XML.save(outputFeedFileName,makeFeed(masterFeed),"UTF-8")
    println("RSS Feed saved to disk; Program Finished")
    // END OF MAIN
  }


  /** create an rss feed file using a Feed object **/
  def makeFeed(feed:Feed):xml.Node = {
    <rss version={"2.0"} xmlns:atom={"http://www.w3.org/2005/Atom"}>
      <channel>
      <atom:link href={channelLink+"/feed.rss"} rel={"self"} type="application/rss+xml" />
      <title>{channelName}</title>
      {if(channelImage != "") {
      <image>
        <url>{channelLink+channelImage}</url>
        <title>{channelImageName}</title>
        <link>{channelLink}</link>
        <width>{channelImageWidth}</width>
        <height>{channelImageHeight}</height>
        </image>
      }}
      <link>{channelLink}</link>
      <description>{channelDesc}</description>
      <generator>{"masspod-rss https://github.com/thall-gnu/masspod-rss"}</generator>
      <docs>{"http://www.rssboard.org/rss-specification"}</docs>
      {
        // spit out the episodes from the current feed
        feed.episodes.map(_.toXML())
      }
      </channel>
      </rss>
  }

  /**
    Function to parse the configuration file.
    Purely side-effecting to change all the private variables used in runtime.
    Matching on initial keywords line by line.
    * */

  def parseConfig(config:List[String]):Unit = {
    for(c <- config){
      val arg = c.substring(c.indexOf(" ")+1)
      c.split(" ")(0) match {
        case "description" => channelDesc = arg
        case "link" => channelLink = arg
        case "title" => channelName = arg
        case "image" => {
          channelImage = arg.split(" ")(0)
          channelImageName = arg.split(" ")(1).trim
          channelImageWidth = arg.split(" ")(2).trim.toInt
          channelImageHeight = arg.split(" ")(3).trim.toInt
        }
        case "filename" => outputFeedFileName = arg
        case "source" => feedSourceFile = arg
        case "directory" => {
          outputDirectory = arg
          if(outputDirectory(0) != '/') Tsys.exitError("Output directory in config is not an absolute file path \n (Must start with \"/\")")
        }
        case "episodeLimit" => oldNum = arg.toInt
        case "titleFilter" => titleFilters = arg :: titleFilters
        case x => {
          val toprint = "unrecognized line in config file: "+x
          Tsys.exitError(toprint)
        }
      }
    }
  }
}
