package masspod_rss

object DateNums {
  /** NOTE:
    Every time "date" is used as an argument for a function
    it refers to the formatting of an Episode's pubDate.
    An example of this formatting is as follows:
    Thu, 29 Mar 2018 12:30:15 GMT
    Per the standard:
    https://www.w3.org/Protocols/rfc822/#z28
    (some feeds don't follow the standard and will not
    prefix single-digit days with a zero)
    * */

  /** 
    The publicly accessible function for getting an "age number"
    associated with an episode to be used for sorting episodes.
    Not meant to be particularly meaningful except relatively with
    other episodes for the purpose of sorting.
    **/
  def dateNum(date:String):Int = calcDate(date)

  /** Bigger is older for both of these **/
  private def calcDate(date:String):Int = {
    val (day,month,year) = dmy(date)
    val absDate = day+(month*30)+(year*365)
    absDate
  }

  // actually parsing the rfc822 date string here
  private def dmy(date:String):(Int,Int,Int) = {
    val dregex = """\p{Alpha}{3}, (\d{1,2}) (\p{Alpha}{3}) (\d{4}) .*""".r
    val (day,month,year) = date match {
      case dregex(day, month, year) => {
        val retDay = if(day.length==1) day else {
          if(day(0) == '0') day.drop(1) else day
        }
        val retMonth = getMonthNum(month)
        (retDay.toInt, retMonth, year.toInt)
      }
    }
    (day,month,year)
  }

  private def getMonthNum(month:String):Int = {
    val ret = month match {
      case "Jan" => 1
      case "Feb" => 2
      case "Mar" => 3
      case "Apr" => 4 // "standards"
      case "May" => 5 // https://xkcd.com/927/
      case "Jun" => 6
      case "Jul" => 7
      case "Aug" => 8
      case "Sep" => 9
      case "Oct" => 10
      case "Nov" => 11
      case "Dec" => 12
    }
    ret
  }
}
