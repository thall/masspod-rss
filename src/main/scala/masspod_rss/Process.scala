package masspod_rss

import scala.xml._
import scala.util._

object Process {
  // get episode sequences from the raw rss feeds
  def processFeed(file:xml.NodeSeq):Seq[Episode] = {
    val feedName = (file \ "channel" \ "title").text
    var episodeSeq:Seq[Episode] = Seq()
    val episodeNodes = (file \ "channel" \ "item")
      episodeNodes.foreach(
        n => {
          val literalLink = (n \ "link").text
          // TODO: Consider replacing this workaround with a regular expression
          val epLink = if(!literalLink.contains("![CDATA")) literalLink else {
            literalLink.drop(8).dropRight(2) // quick and dirty workaround
          }
          val title = (n \ "title").text
          // url varies based on the rss specification
          val url = if(!(n \ "enclosure" \ "@type").text.contains("audio")) {
            epLink
          } else {
            val one = (n \ "enclosure" \ "@url").text
            val ret = if(one.contains("?dest")){
              one.splitAt(one.indexOf("?dest"))._1
            } else one
            ret
          }
          val length:Long = if(!(n \ "enclosure" \ "@length").text.contains("audio")) {
	          0L
	        } else (n \ "enclosure" \ "@length").text.toLong
          val filetype:String = if(!(n \ "enclosure" \ "@type").text.contains("audio")) {
            null
          } else (n \ "enclosure" \ "@type").text
          val description = (n \ "description").text
          val pubDate = (n \ "pubDate").text
          val extension = "."+url.drop(url.lastIndexOf(".")+1)
          // create the episode
          val newEp = new Episode(feedName, title, url, description, pubDate, epLink, extension, length,filetype)
          // add it to the sequence
          episodeSeq = episodeSeq :+ newEp
        })
    episodeSeq
  }

  private def sortByPubDate(e1:Episode,e2:Episode) = {
    DateNums.dateNum(e1.pubDate) > DateNums.dateNum(e2.pubDate)
  }

  def sortByDate(episodes:Seq[Episode]):Seq[Episode] = {
    episodes.sortWith(sortByPubDate)
  }
}
