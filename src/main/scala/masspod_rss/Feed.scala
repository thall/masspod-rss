package masspod_rss

import sys.process._
import scala.xml.NodeSeq

class Feed(val name:String, val feedLink:String, private var _episodeSeq:Seq[Episode], val outputURL:String){
  def title(): String = name

  def fTitle: String = {
    this.title.replaceAll("-","").replaceAllLiterally("""/""","_").replaceAll(" ", "_").replaceAll("\"","")
  }

  def feedUrl(): String = feedLink

  /**
    Just to be clear here: All filepaths should be absolute filepaths;
    if the config parser finds a non-absolute filepath (doesn't start at root)
    the program will exit with an error.
    **/

  def downloadAll(outputDir:String,channelLink:String): Unit = {
    this.episodes.par.foreach(
      e => {
        // sort out where the file should be located on the disk
        val dirString = outputDir+"/"+e.feedFTitle+"/"
        val feedDirectory = new java.io.File(dirString)
        // this disk file access is a race condition, but it is harmless since it will just fail
        // gracefully if it already exists from another thread
        feedDirectory.mkdir()
        val diskFileLocation = dirString+e.fTitle+e.extension
        /**
          inform the episode of its filepath on disk
          this would be the output directory followed by
          a folder for its source feed's title
          * */
        e.setDlDir(diskFileLocation)
        val fileOnDisk = new java.io.File(diskFileLocation)
        if(!fileOnDisk.exists) {
          // download dat episode
          this.dlFile(e.mediaUrl,e.getDlDir)
        } else {
          /**
            Length can be null if the source feed is terrible so we have to check for that.
            This runs when the file length of what is already downloaded isn't matching
            with what is available from the refreshed feed. We delete the current file and try downloading it again.
            This should never have runtime errors because we would have already ran into
            file permissions issues by this point if that was the case, hence no checking.
            **/
          fileOnDisk.delete()
          this.dlFile(e.mediaUrl,e.getDlDir)
        }
        // set file length
        e.setLength(fileOnDisk.length())
        // figure out what our URL link to the episode should be
        val webFileLocation = channelLink+e.feedFTitle+"/"+fileOnDisk.getName
        e.setExternalDlDir(webFileLocation)
        print("#")
      }
    )
  }

  def episodes(): Seq[Episode] = _episodeSeq

  def filterEps(oldNum:Int, filterList:List[String]): Feed = {
    this.removeFiltered(filterList)
    this.removeOld(oldNum)
    this
  }

  // remove old episodes
  def removeOld(oldNum:Int): Unit = {
    if(this.episodes().length > oldNum) {
      _episodeSeq = this.episodes.take(oldNum)
    }
  }

  def removeFiltered(filterList:List[String]): Unit = for (ep <- this.episodes(); filter <- filterList; if (ep.getTitle.contains(filter))) removeEp(ep)

  /** This will also remove the copy of the file on the disk if it exists from a previous run **/
  private def removeEp(ep:Episode): Unit = {
    // check whether relative directory or specified in config
    val dirPrefix = if(this.outputURL.length<1) "" else this.outputURL
    val possibleDlDir = dirPrefix+this.fTitle+"/"+ep.fTitle+"."+ep.extension
    // does it already exist on the filesystem?
    val epOnDisk = new java.io.File(possibleDlDir)
    val epExists = epOnDisk.exists
    // remove the file
    if(epExists) epOnDisk.delete()
    _episodeSeq = this.episodes().filterNot(x => x==ep)
  }

  private def dlFile(url:String,filepath:String):Unit = {
    val cmd = "wget "+url+" --output-document="+filepath+" -q --random-wait"
    cmd .!
  }

}
