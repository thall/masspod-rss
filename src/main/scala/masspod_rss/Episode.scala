package masspod_rss

class Episode(val feed:String,
              val title:String,
              private var _mediaUrl:String,
              val description:String,
              private var _pubDate:String,
              val epLink:String,
              private var _extension:String,
              private var _length:Long,
              private var _type:String)
{
  /** where the episode is saved to disk **/
  private var dlDir:String = null
  private var externalDlDir:String = null

  def length():Long = _length
  def setLength(newLength:Long):Unit = {
    _length = newLength
  }

  def filetype():String = _type
  def getTitle():String = this.title

  // internal is for location on the disk
  def getDlDir():String = dlDir

  def setDlDir(dir:String):Unit = {
    dlDir = dir
  }

  // external is for the download url in the rss
  def getExternalDlDir():String = externalDlDir

  def setExternalDlDir(dir:String):Unit = {
    externalDlDir = dir
  }

  def mediaUrl = _mediaUrl

  def pubDate = _pubDate

  def extension = _extension

  /**
    Make a friendly filename from an episode title
    If you are confused about why I chose to
    have a hash as an else see: http://www.hellointernet.fm/podcast/101
    Thank you for breaking my aggregator, Grey.
    **/
  def fTitle:String = {
    val defaultTitle = this.title.filter(_.isLetterOrDigit)
    if(defaultTitle.length>0) defaultTitle else {
      // Somebody decided they hate me so I'm hashing their title (just the filename lol)
      val hasher = new scala.util.hashing.ByteswapHashing[String]
      hasher.hash(this.title).toString
    }
  }

  def feedFTitle:String = {
    val defaultTitle = this.feed.filter(_.isLetterOrDigit)
    if(defaultTitle.length>0) defaultTitle else {
      // Somebody decided they hate me so I'm hashing their title (just the filename lol)
      val hasher = new scala.util.hashing.ByteswapHashing[String]
      hasher.hash(this.feed).toString
    }
  }

  def toXML() = {
    // the if statement is for dropping the leading '/' if the output dir is relative
    val dLurl = this.getExternalDlDir
    <item>
      <title>{this.title+"-"+this.feed}</title>
      <link>{dLurl}</link>
      <guid>{dLurl}</guid>
      <pubDate>{this.pubDate}</pubDate>
      <description>{"LINK TO ORIGINAL CONTENT:"+this.epLink+"\n"+this.description}</description>
      <enclosure url={dLurl} length={this.length.toString} type={if(this.filetype()==null) "audio/mpeg" else this.filetype()}/>
    </item>
  }
}
