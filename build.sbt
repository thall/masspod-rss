name := "MasspodRSS"
version := "1.1"
scalaVersion := "2.12.4"

// added to allow assembly to work
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

// experience errors like the end-user would, dont' fork the JVM
trapExit := false

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-xml" % "1.0.6",
  "org.scalactic" %% "scalactic" % "3.0.4",
)
