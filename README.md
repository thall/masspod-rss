```
 __  __                               _ ____  ____ ____  
|  \/  | __ _ ___ ___ _ __   ___   __| |  _ \/ ___/ ___| 
| |\/| |/ _` / __/ __| '_ \ / _ \ / _` | |_) \___ \___ \ 
| |  | | (_| \__ \__ \ |_) | (_) | (_| |  _ < ___) |__) |
|_|  |_|\__,_|___/___/ .__/ \___/ \__,_|_| \_\____/____/ 
                     |_|                                 
```                    

# masspod-rss
A dead-simple RSS-Feed aggregator and downloader script written specifically for Podcasts

- Designed to create a locally hosted, aggregated podcast RSS feed quickly using concurrency.
- Returns a `0` upon success and a `1` upon failure to allow for easily scripting feed updates or even possibly to be used like a system-service.
 
GPLv3(c) Turner Hall - 2018 --
Download the latest version from the releases directory in the repository or build yourself with `sbt assembly`.

------
## Dependencies:
- Linux/MaxOSX
- wget
- scala/java8

------
## usage:
`$ masspod-rss feed.config`
Where `feed.config` is a configuration file formatted like the example in this repo.

... and in addition `sources.txt` (from the config) is a newline-seperated list of podcast feed .rss urls like below:
```
http://www.hellointernet.fm/podcast?format=rss
https://linuxunplugged.fireside.fm/rss

```
... make certain your sources.txt file does not end with an empty line and has no extra whitespace.

An example of a simple script that could be run with cron to automate feed updates is below:
```
#!/bin/bash
update="scala /path/to/MasspodRSS.jar /path/to/feed.config"
# Update the feed and record a log of successes or failures
# based on exit status (0 and 1 for success and failure)
if $update; then
 echo "rss update at: $(date)" >>"/path/to/autoruns.log" 2>&1
else    
 echo "ERROR! at $(date)" >>"/path/to/autoruns.log" 2>&1
fi
```

## config options: (also reference the example configuration in the repository)
### Note: all files must have a full filepath to the file starting with a forward slash
- `title` The title of the feed
- `description` The description of your feed (may or may not show up in the RSS feed reader)
- `link` is the url your feed will be available from and should correspond with `directory`. Must end with a `"/"`.
- `image` defines a small image that displays with your feed in certain RSS readers (like AntennaPod) by its `filename` `image-title` `width-in-px` `height-in-px` all seperated by spaces. The rss specification sets a very small limit on the image size, keep it below about 150x90px.
- `filename` and `source` are the filename/path for your feeds inputs and outputs respectively (`out.rss`, `/path/to/sources.txt`). These do not inherit from the `directory` config option's path so make sure to specify a full path unless you would like the files to be saved relative to the current directory.
- `directory` is the path of the directory to which audio files will be saved, make sure this is somewhere a remote client has permissions to access files from.
- `episodeLimit` (1.1 onwards) specifies an max integer number of episodes to download into the aggregated feed; these will be the most recent
- `expiration` (1.0 only; deprecated) Defines how new a podcast episode or entry must be to be included in the final feed in terms of days. The default is 80 and episodes older than 80 days will not be shown in the output feed.
- `titleFilter` Define a filter based on the content of episode titles. The filter text starts after a space. By using `titleFilter` on multiple lines you can define any number of title filters you need.
